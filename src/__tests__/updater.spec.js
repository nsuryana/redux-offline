import { offlineUpdater } from '../updater';
import { PERSIST_REHYDRATE } from '../constants';

describe('offlineUpdater', () => {
  describe('when rehydrated', () => {
    const prevState = {
      outbox: [],
      busy: true,
      online: true
    };

    const rehydrateOfflineAction = {
      type: PERSIST_REHYDRATE,
      payload: {
        offline: {
          outbox: [{ data: 'queue-item-1' }, { data: 'queue-item-1' }],
          busy: true,
          online: false
        }
      }
    };

    const rehydrateMissingOfflineAction = {
      type: PERSIST_REHYDRATE,
      payload: {}
    };

    it('restores the outbox', () => {
      const newState = offlineUpdater(prevState, rehydrateOfflineAction);
      expect(newState.outbox).toEqual([{ data: 'queue-item-1' }, { data: 'queue-item-1' }]);
    });

    it('sets busy to false', () => {
      const newState = offlineUpdater(prevState, rehydrateOfflineAction);
      expect(newState.busy).toEqual(false);
    });

    it('does not update other offline fields', () => {
      const newState = offlineUpdater(prevState, rehydrateOfflineAction);
      expect(newState.online).toEqual(true);
    });

    it('does not change outbox if offline is not present', () => {
      const newState = offlineUpdater(prevState, rehydrateMissingOfflineAction);
      expect(newState.busy).toEqual(false);
      expect(newState.outbox).toEqual(prevState.outbox);
    });
  });
});
